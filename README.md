# kalkulilo

Scientific calculator

## Screen shots
![Form](screenshoot.png)

## History
This calculator was started in order to learn how to use the Eval component of Gambas3.
Over time I hope the capacity will increase.
I hope you find it interesting to experiment with this code and of course do not hesitate to suggest improvements or report errors.

## Features
Nowadays it is used to do basic calculations. Add, subtract, multiply, divide and some more.

### Notes for rigonometric functions

 - The angle is specified in radians.

## References
This program has been developed with Gambas IDE.
http://gambas.sourceforge.net/en/main.html