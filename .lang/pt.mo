��          �      l      �  
   �     �     �  
          	             3     G     ^     p     �     �     �  
   �     �     �     �     �  #   �  �        �  	   �               $     +     8     T     k     �     �     �     �  %   �     �                     +  )   4                     	                                                      
                       Arc cosine Arc sine Arc tangent Calculator Cosine Cube root Hyperbolic arc cosine Hyperbolic arc sine Hyperbolic arc tangent Hyperbolic cosine Hyperbolic sine Hyperbolic tangent Office Put the number first  Scientific Scientific calculator Sine Square root Tangent The last term cannot be an operator Project-Id-Version: kalkulilo 3.15.2
PO-Revision-Date: 2021-05-15 20:27 UTC
Last-Translator: belmotek <info@belmotek.net>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Arco cosseno Sine arco Tangente de arco Calculadora Cosine Raiz de cubo Cosine do arco hiperbólico Arco seno hiperbólico Arco tangente hiperbólico Cosina hiperbólica Seno hiperbólico Tangente hiperbólica Escritório Coloque o número em primeiro lugar.  Científico Calculadora científica Sine Raiz quadrada Tangente O último termo não pode ser um operador 