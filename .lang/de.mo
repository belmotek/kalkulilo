��          �      l      �  
   �     �     �  
          	             3     G     ^     p     �     �     �  
   �     �     �     �     �  #   �  �        �  
   �     �                    (     D     ^     z     �     �     �  (   �     �  !   �          %     3  *   <                     	                                                      
                       Arc cosine Arc sine Arc tangent Calculator Cosine Cube root Hyperbolic arc cosine Hyperbolic arc sine Hyperbolic arc tangent Hyperbolic cosine Hyperbolic sine Hyperbolic tangent Office Put the number first  Scientific Scientific calculator Sine Square root Tangent The last term cannot be an operator Project-Id-Version: Calculator 0.0.1
PO-Revision-Date: 2021-05-13 15:39 UTC
Last-Translator: Tradukisto
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Bogenkosine Bogensinus Bogentangente Taschenrechner Kosinus Würfelwurzel Hyperbolischer Bogenkosinus Hyperbolischer Bogensinus Hyperbolische Bogentangente Hyperbolischer Kosinus Hyperbolischer Sinus Hyperbolische Tangente Büro Setzen Sie die Zahl an die erste Stelle  Wissenschaftlich Wissenschaftlicher Taschenrechner Sinus Quadratwurzel Tangente Der letzte Begriff kann kein Operator sein 