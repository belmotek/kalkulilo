��          �      l      �  
   �     �     �  
          	             3     G     ^     p     �     �     �  
   �     �     �     �     �  #   �  �        �  	   �     �                    (     A     \     z     �     �     �     �     �     �          	       0   !                     	                                                      
                       Arc cosine Arc sine Arc tangent Calculator Cosine Cube root Hyperbolic arc cosine Hyperbolic arc sine Hyperbolic arc tangent Hyperbolic cosine Hyperbolic sine Hyperbolic tangent Office Put the number first  Scientific Scientific calculator Sine Square root Tangent The last term cannot be an operator Project-Id-Version: kalkulilo 3.15.2
PO-Revision-Date: 2021-05-13 15:43 UTC
Last-Translator: Tradukisto
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Cosine d’arc Arc sinus Arc tangente Calculatrice Cosinus racine cubique Arc cosinus hyperbolique Sinus d’arc hyperbolique Tangente d’arc hyperbolique cosinus hyperbolique Sinus hyperbolique Tangente hyperbolique bureau Mettez le numéro en premier  Scientifique Calculateur scientifique sinus racine carrée tangente Le dernier terme ne peut pas être un opérateur 