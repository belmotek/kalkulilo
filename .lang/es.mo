��          �      l      �  
   �     �     �  
          	             3     G     ^     p     �     �     �  
   �     �     �     �     �  #   �  �        �  	   �                     '     5     Q     k     �     �     �     �     �  
   �     �                 ,   #                     	                                                      
                       Arc cosine Arc sine Arc tangent Calculator Cosine Cube root Hyperbolic arc cosine Hyperbolic arc sine Hyperbolic arc tangent Hyperbolic cosine Hyperbolic sine Hyperbolic tangent Office Put the number first  Scientific Scientific calculator Sine Square root Tangent The last term cannot be an operator Project-Id-Version: kalkulilo 3.15.2
PO-Revision-Date: 2021-05-06 12:40 UTC
Last-Translator: belmotek <info@belmotek.net>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Arco coseno Arco seno Arco tangente Calculadora Coseno Raíz cúbica Coseno de arco hiperbólico Seno de arco hiperbólico Arco tangente hiperbólico Coseno hiperbólico Seno hiperbólico Tangente hiperbólica Oficina Ponga el número primero Cientifico Calculadora científica Seno Raíz cuadrada Tangente El último término no puede ser un operador 