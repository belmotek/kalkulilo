��          �      l      �  
   �     �     �  
          	             3     G     ^     p     �     �     �  
   �     �     �     �     �  #   �  �        �     �     �                     .     D     \     x     �     �     �     �     �     �     �     �       -                        	                                                      
                       Arc cosine Arc sine Arc tangent Calculator Cosine Cube root Hyperbolic arc cosine Hyperbolic arc sine Hyperbolic arc tangent Hyperbolic cosine Hyperbolic sine Hyperbolic tangent Office Put the number first  Scientific Scientific calculator Sine Square root Tangent The last term cannot be an operator Project-Id-Version: Calculator 0.0.1
PO-Revision-Date: 2021-05-13 15:43 UTC
Last-Translator: Tradukisto
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Coseno ad arco Arco sinusoidale Arco tangente Calcolatrice coseno radice cubica Arcocoseno iperbolico Seno ad arco iperbolico Tangente ad arco iperbolico Coseno iperbolico Seno iperbolico Tangente iperbolica Ufficio Metti prima il numero Scientifico Calcolatrice scientifica Sine radice quadrata Tangente L'ultimo termine non può essere un operatore 